import Login from "../components/Login.vue";
import Register from "../components/Register.vue";
import Awal from "../view/Awal.vue";
import Admin from "../view/Admin.vue";
import User from "../view/User.vue";
import Data from "../view/Data.vue";
import Menu from "../view/Menu.vue";
import Cart from "../view/Cart.vue";
import Sidebar from "../components/Sidebar.vue";
import Makananberat from "../data/Makanan.vue";
import Makananringan from "../data/MknnRingan.vue";
import Minuman from "../data/Minuman.vue";
import Obat from "../data/Obat.vue";
import Buku from "../data/Buku.vue";
import Alat from "../data/AlatTulis.vue";
import Sembako from "../data/Sembako.vue";
import Mainan from "../data/Mainan.vue";
import Siswa from "../data/Siswa.vue";
import Perlengkapan from "../data/Perlengkapan.vue";

import Vue from "vue";
import Router from "vue-router"


Vue.use(Router)
const routes =[
    // router login
    {
        path: "/login",
        name: "Login",
        component: Login,
    },
    // router register
    {
        path: "/register",
        name: "Register",
        component: Register,
    },
    {
        path: "/",
        name: "AwalPage",
        component: Awal,
    },
    {
        path: "/admin",
        name: "Admin",
        component: Admin,
    },
    {
        path: "/data",
        name: "Data",
        component: Data,
    },
    {
        path: "/menu",
        name: "MenuPage",
        component: Menu,
    },
    {
        path: "/sidebar",
        name: "SidebarPage",
        component: Sidebar,
    },
    {
        path: "/makananberat",
        name: "MakananPage",
        component: Makananberat,
    },
    {
        path: "/makananringan",
        name: "MakananringanPage",
        component: Makananringan,
    },
    {
        path: "/minuman",
        name: "MinumanPage",
        component: Minuman,
    },
    {
        path: "/obat",
        name: "ObatPage",
        component: Obat,
    },
    {
        path: "/buku",
        name: "BukuPage",
        component: Buku,
    },
    {
        path: "/alattulis",
        name: "AlatPage",
        component: Alat,
    },
    {
        path: "/sembako",
        name: "SembakoPage",
        component: Sembako,
    },
    {
        path: "/mainan",
        name: "MainanPage",
        component: Mainan,
    },
    {
        path: "/siswa",
        name: "SiswaPage",
        component: Siswa,
    },
    {
        path: "/perlengkapan",
        name: "PelengkapanPage",
        component: Perlengkapan,
    },
    {
        path: "/user",
        name: "UserPage",
        component: User,
    },
    {
        path: "/cart",
        name: "CartPage",
        component: Cart,
    }
]

const router = new Router({
    routes,
    mode: "history",
});

export default router;